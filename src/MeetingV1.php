<?php
namespace Nsru\Meeting;

use Exception;

class MeetingV1
{
    private $eMeetingV1Base = 'https://e-meeting.nsru.ac.th/';
    private $appId          = "";
    private $searchToken    = null;

    public function __construct($appId)
    {
        $this->appId = $appId;
    }

    /**
     * กำหนด Token สำหรับใช้งาน API ของระบบ e-meeting (สำหรับการ Reuse เพื่อลดขั้นตอนในการเรียกใช้งาน Service)
     *
     * @param string $searchToken Token สำหรับการเรียกใช้ API
     * @return void
     */
    public function setSearchToken($searchToken) : void
    {
        $this->searchToken = $searchToken;
    }

    /**
     * ขอ Token สำหรับใช้งาน API ของระบบ e-meeting
     *
     * @return string
     */
    public function getSearchToken() : string
    {
        try {
            if($this->searchToken != null) {
                return $this->searchToken;
            } else {
                $url = $this->eMeetingV1Base."api/get/search-token/".$this->appId;
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, $url);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
                curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
                $response = curl_exec($ch);
        
                if(curl_errno($ch)) {
                    throw new Exception(curl_error($ch));
                } else {
                    if($response) {
                        $response = json_decode($response);
                        if($response->tokenStatus) {
                            $this->setSearchToken($response->searchToken);
                            return $response->searchToken;
                        } else throw new Exception($response->message);
                    } else throw new Exception('No response from server');
                }
            }
        } catch( Exception $e ) {
            return false;
        }
    }

    /**
     * ดึงรายการประชุมทั้งหมด
     *
     * @param string $nsruAccountUsername ชื่อผู้ใช้งานระบบ e-meeting
     * @param string $date วันที่ต้องการดึงข้อมูล รูปแบบ Y-m-d
     * @return array|false
     */
    public function getMeetingList($nsruAccountUsername, $date = null)
    {
        try {
            $searchToken = $this->getSearchToken();

            if($date) {
                $url = $this->eMeetingV1Base."api/get/meeting-list/{$searchToken}/{$nsruAccountUsername}/{$date}";
            } else {
                $url = $this->eMeetingV1Base."api/get/meeting-list/{$searchToken}/{$nsruAccountUsername}";
            }
                
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $url);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, false );
            curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, false );
            $response = curl_exec($ch);
        
            if(curl_errno($ch)) {
                throw new Exception(curl_error($ch));
            } else {
                if($response) {
                    $response = json_decode($response);
                    if($response->status == true) {
                        return (array)$response->event;
                    } else throw new Exception($response->message);
                } else throw new Exception('No response from server');
            }
        } catch( Exception $e ) {
            return false;
        }
    }

    /**
     * ดึงข้อมูลกาประชุมในวันนี้
     *
     * @param string $nsruAccountUsername ชื่อผู้ใช้งานระบบ e-meeting (NSRU Account Username)
     * @return array|false
     */
    public function getTodayMeetingList($nsruAccountUsername)
    {
        try {
            $currentDate = date('Y-m-d');
            $meetings = $this->getMeetingList($nsruAccountUsername, $currentDate);
            return $meetings[$currentDate] ?? [];
        } catch( Exception $e ) {
            return false;
        }
    }

    /**
     * ดึงข้อมูลการประชุมที่ยังไม่เกิดขึ้น
     *
     * @param string $nsruAccountUsername ชื่อผู้ใช้งานระบบ e-meeting (NSRU Account Username)
     * @return array|false
     */
    public function getFutureMeetingList($nsruAccountUsername)
    {
        try {
            $currentDate = date('Y-m-d');
            $meetings = $this->getMeetingList($nsruAccountUsername);
            foreach($meetings as $date => $dayMeetings) {
                if($date <= $currentDate) {
                    unset($meetings[$date]);
                }
            }
            return $meetings;
        } catch( Exception $e ) {
            return false;
        }
    }
   
}